package nl.bioinf.rvandepol.hello;

//many imports

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        //fetch the session object
        //if it is not present, one will be created
        HttpSession session = request.getSession();
        String nextPage;

        if (session.isNew() || session.getAttribute("user") == null) {
            MockDatabase checkCredentials = new MockDatabase();
            boolean authenticated = checkCredentials.checkCredentials(username,password);

            if (authenticated) {
                session.setAttribute("user", new User(username, User.Role.ADMIN));
//                ctx.setVariable("user", new User(username, User.Role.valueOf(username)));
                nextPage = "dogbreeds";
                response.sendRedirect("/dogbreeds");
//                voor het controleren van de rights een switch/case gebruiken icm de user enum
            } else {
                ctx.setVariable("message", "Your password and/or username are incorrect; please try again");
                ctx.setVariable("message_type", "error");
                nextPage = "login";
            }
        } else {
            nextPage = "login";
        }
        templateEngine.process(nextPage, ctx, response.getWriter());
    }

//    private boolean authenticate(String username, String password) {
//        return username.equals("rvandepol") && password.equals("testing");
//    }

    //simple GET requests are immediately forwarded to the login page
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Object logout = request.getParameter("logout");
        if(logout != null) {
            session.removeAttribute("user");
            response.sendRedirect("/login");
        }
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("login", ctx, response.getWriter());
    }
}
