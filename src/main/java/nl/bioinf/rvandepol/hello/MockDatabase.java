package nl.bioinf.rvandepol.hello;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MockDatabase {

//    public static void main(String[] args) {
//        MockDatabase tester = new MockDatabase();
//        tester.checkCredentials("rvandepol", "testing");
//    }

    public boolean checkCredentials (String username, String password) {
        HashMap<String, String> members = new HashMap<>();
        HashMap<String, List<String>> test = new HashMap<>();
        ArrayList<String> rvandepol = new ArrayList<>();
        ArrayList<String> Henk = new ArrayList<>();
        ArrayList<String> Frits = new ArrayList<>();
        rvandepol.add(0, "testing");
        rvandepol.add(1, "ADMIN");
        Henk.add(0, "henk");
        Henk.add(1, "USER");
        Frits.add(0, "frits");
        Frits.add(1, "GUEST");
        test.put("rvandepol", rvandepol);
        test.put("Henk", Henk);
        test.put("Frits", Frits);
        try {
//            boolean ismember = test.get(username).equals(password);
            boolean isMember = test.get(username).get(0).equals(password);
//            boolean isMember = members.get(username).equals(password);
            return isMember;
        }
        catch (NullPointerException e) {
            return false;
        }
    }
}
