package nl.bioinf.rvandepol.hello;

import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

//@WebServlet(name = "HelloServlet", urlPatterns = "/welcome", loadOnStartup = 1)
public class HelloServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        //this step is optional; standard settings also suffice
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("currentDate", new Date());

        Bird bird = new Bird("clanga", "Aquila", "lesser spotted eagle");
        ctx.setVariable("bird", bird);


        final ServletContext servletContext  = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("welcome", ctx, response.getWriter());
    }
}
