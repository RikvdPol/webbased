package nl.bioinf.rvandepol.hello;

import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet(name = "HeartRateServlet", urlPatterns = "/heartrate", loadOnStartup = 1)
public class HeartRateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int heartrate = Integer.parseInt(request.getParameter("heartrate"));
        HeartRateCalculator heartRateCalculator = new HeartRateCalculator(heartrate);

        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        ctx.setVariable("heartrate", heartRateCalculator);
        final ServletContext servletContext  = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("heartrateResults", ctx, response.getWriter());

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("currentDate", new Date());

        final ServletContext servletContext  = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("heartrate", ctx, response.getWriter());
    }
}
