package nl.bioinf.rvandepol.hello;

public class User {
    private String name;
    private String email;
    private String password;
    private Role role;

    public User() {
    }

    public User(String name, Role role) {
        this.name = name;
//        this.email = email;
        this.role = role;
    }

    public User(String name, String email, String password, Role role) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public enum Role {
//        Henk("Guest"),
//        Frits("User"),
//        rvandepol("Admin");
        GUEST,
        USER,
        ADMIN;

//        private String role;
//
//        Role(String role) {
//            this.role = role;
//        }
//
//        @Override
//        public String toString() {
//            return role;
//        }
    }

}

