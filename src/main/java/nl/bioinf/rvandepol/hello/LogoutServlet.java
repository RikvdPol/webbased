package nl.bioinf.rvandepol.hello;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(urlPatterns = "/logout")
public class LogoutServlet extends HttpServlet{
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.removeAttribute("username");
        request.removeAttribute("password");
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        //fetch the session object
        //if it is not present, one will be created
//        HttpSession session = request.getSession();
//        String nextPage;


        templateEngine.process("login", ctx, response.getWriter());

//        final ServletContext servletContext  = super.getServletContext();
//        WebConfig.createTemplateEngine(servletContext).process(nextPage, ctx, response.getWriter());
    }
}
