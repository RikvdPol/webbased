package nl.bioinf.rvandepol.hello;

import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(name = "DogBreedsServlet", urlPatterns = "/dogbreeds", loadOnStartup = 1)
public class DogBreedsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filePath = getServletContext().getInitParameter("File");
        DogData test = new DogData();
        List<DogInfo> species = test.speciesCreator(filePath);
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        ctx.setVariable("species", species);
        HttpSession session = request.getSession();
        BrowsingHistory history = (BrowsingHistory) session.getAttribute("history");
        Object testone = session.getAttribute("user");

        if(testone == null) {
            response.sendRedirect("/login");
        }

        if(history != null) {
            history.addItem("Home");
            session.setAttribute("history", history);
        }
        else {
            BrowsingHistory browsingHistory = new BrowsingHistory();
            browsingHistory.addItem("Home");
            session.setAttribute("history", browsingHistory);
        }

        ctx.setVariable("history", history);
        final ServletContext servletContext  = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("listing", ctx, response.getWriter());

    }
}
