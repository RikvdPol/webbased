package nl.bioinf.rvandepol.hello;

public class Bird {
    private String speciesName;
    private String genusName;
    private String englishName;

    public Bird() {}

    public Bird(String speciesName, String genusName, String englishName) {
        this.speciesName = speciesName;
        this.genusName = genusName;
        this.englishName = englishName;
    }

    public String getSpeciesName() {
        return speciesName;
    }

    public String getGenusName() {
        return genusName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getScientificName() {
        return genusName + " " + speciesName;    }
}
