package nl.bioinf.rvandepol.hello;

public class HeartRateCalculator {
    private int heartRate;

    public HeartRateCalculator() {}

    public HeartRateCalculator(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public int getZoneOne() {
        return (int) (heartRate * 0.5);
    }

    public int getZoneTwo() {
        return (int) (heartRate * 0.6);
    }

    public int getZoneThree() {
        return (int) (heartRate * 0.7);
    }

    public int getZoneFour() {
        return (int) (heartRate * 0.8);
    }

    public int getZoneFive() {
        return (int) (heartRate * 0.9);
    }
}
