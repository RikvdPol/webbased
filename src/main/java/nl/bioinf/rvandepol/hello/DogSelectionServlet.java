package nl.bioinf.rvandepol.hello;

import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

@WebServlet(name = "dogSelectionServlet", urlPatterns = "/dog.info", loadOnStartup = 1)
public class DogSelectionServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        String dogobject = request.getParameter("dogobject");
        String[] dogs = dogobject.split(";");
        String dogScientificName = dogs[0];
        String dogEnglishName = dogs[1];
        String dogDutchName = dogs[2];
        String dogAge = dogs[3];
        String photoPath = dogs[4];
        ctx.setVariable("scientificName", dogScientificName);
        ctx.setVariable("englishName", dogEnglishName);
        ctx.setVariable("dutchName", dogDutchName);
        ctx.setVariable("age", dogAge);
        ctx.setVariable("photoPath", photoPath);

        HttpSession session = request.getSession();
        BrowsingHistory history = (BrowsingHistory) session.getAttribute("history");

        if(history != null) {
            history.addItem(dogEnglishName);
            session.setAttribute("history", history);
        }
        else {
            BrowsingHistory browsingHistory = new BrowsingHistory();
            browsingHistory.addItem(dogEnglishName);
            session.setAttribute("history", browsingHistory);
        }
        ctx.setVariable("history", history);
        final ServletContext servletContext  = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("dogbreeds", ctx, response.getWriter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        final ServletContext servletContext  = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("dogbreeds", ctx, response.getWriter());
    }

}
