package nl.bioinf.rvandepol.hello;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DogData  {
    public static Map<String, DogInfo> DOG_DATA = new HashMap<>();


    public List<DogInfo> speciesCreator (String filePath) {
//    static {
        //read file
        File inFile = new File(filePath);
        List<DogInfo> species = new ArrayList<>();
        try {
            BufferedReader br;
            String sCurrentLine;

            br = new BufferedReader(new FileReader(inFile));

            while ((sCurrentLine = br.readLine()) != null) {
                if(sCurrentLine.startsWith("scientific_name")) continue;
                String scientificName = sCurrentLine.split(";")[0];
                String englishName = sCurrentLine.split(";")[1];
                String dutchName = sCurrentLine.split(";")[2];
                String avarageAge = sCurrentLine.split(";")[3];
                String photePath = sCurrentLine.split(";")[4];
                DogInfo speciesTable = new DogInfo(scientificName, englishName, dutchName, avarageAge, photePath);
                species.add(speciesTable);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return species;
    }

}
