package nl.bioinf.rvandepol.hello;

public class DogInfo {
    private String scientificName;
    private String englishName;
    private String dutchName;
    private String averageAge;
    private String photoPath;

    public DogInfo() {}

    public DogInfo(String scientificName, String englishName, String dutchName, String averageAge, String photoPath) {
        this.scientificName = scientificName;
        this.englishName = englishName;
        this.dutchName = dutchName;
        this.averageAge = averageAge;
        this.photoPath = photoPath;
    }

    public String getScientificName() {
        return scientificName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getDutchName() {
        return dutchName;
    }

    public String getAverageAge() {
        return averageAge;
    }

    public String getPhotoPath() {
        return photoPath;
    }
}
